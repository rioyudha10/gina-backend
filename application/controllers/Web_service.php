<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_service extends CI_Controller {
	private $mysqli_link;
	public function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta"); 
		$this->load->model('Api_model');
		$this->keyAPI = '350ea7e2af60c9d3824791dd122272d8';
		//$this->mysqli_link = mysqli_connect("localhost", "root", "Billing2014", "g_simpok_api");
	}
	public function Api(){
		if($this->input->post('keyapi') == $this->keyAPI){
			$keyapi = $this->input->post('keyapi');
			$dataapi = $this->input->post('dataapi');
			switch($dataapi){
				case '0' : //TEST
					$title = $this->input->post('title');
					$value = $this->oneSignal($title);
					echo $value;
				break;
				case '1' : //CHECK IN 
					if(isset($_FILES['image']['name'])){
						$caption = $this->input->post('caption');
						$lat = $this->input->post('latitude');
						$long = $this->input->post('longitude');
						$nik = $this->input->post('nik');
						if($this->input->post('latitude') || $this->input->post('longitude') || $this->input->post('caption') || 
							$this->input->post('nik')){
							$value = $this->CheckIn($_FILES['image']['name'], $caption, $lat, $long, $nik);
						} else{
							$rst = array('rest' => '0', 'info' => 'Application Param Error');
							$rst = json_encode($rst);
							$value = $rst;
						}
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Error Image Not Found');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '21' : //GET ALL PESAN MASUK PORGRESS
					if($this->input->post('nik') || $this->input->post('level')){
						$nik = $this->input->post('nik');
						$level = $this->input->post('level');
						$value = $this->getAbsenPesanMasukProgress($nik,$level);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '22' : //GET ALL PESAN MASUK HISTORY
					if($this->input->post('nik') || $this->input->post('level')){
						$nik = $this->input->post('nik');
						$level = $this->input->post('level');
						$value = $this->getAbsenPesanMasukHistory($nik,$level);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '3' : //BACA PESAN MASUK
					if($this->input->post('nik') && $this->input->post('id_pesan') && $this->input->post('own') != NULL && $this->input->post('source') != NULL && $this->input->post('type') && $this->input->post('id_type') && $this->input->post('jenis_pesan') != NULL){
						$nik = $this->input->post('nik');
						$id_pesan = $this->input->post('id_pesan');
						$own = $this->input->post('own');
						$source = $this->input->post('source');
						$type = $this->input->post('type');
						$id_type = $this->input->post('id_type');
						$jenis_pesan = $this->input->post('jenis_pesan');
						$value = $this->viewAbsenPesanMasuk($nik,$id_pesan,$own,$source,$type,$id_type,$jenis_pesan);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
					
				break;
				case '4' : //PROSES PESAN MASUK
					if($this->input->post('nik') && $this->input->post('id_pesan') && $this->input->post('approval') != NULL && $this->input->post('id_type') && $this->input->post('type')){
						$nik = $this->input->post('nik');
						$id_pesan = $this->input->post('id_pesan');
						$approval = $this->input->post('approval');
						$id_type = $this->input->post('id_type');
						$type = $this->input->post('type');
						$value = $this->absenPesanMasukProses($nik,$id_pesan,$approval,$id_type,$type);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '5' : //CHECK OUT 
					if(isset($_FILES['image']['name'])){
						$caption = $this->input->post('caption');
						$lat = $this->input->post('latitude');
						$long = $this->input->post('longitude');
						$nik = $this->input->post('nik');
						if($this->input->post('latitude') || $this->input->post('longitude') || $this->input->post('caption') || 
							$this->input->post('nik')){
							$value = $this->CheckOut($_FILES['image']['name'], $caption, $lat, $long, $nik);
						} else{
							$rst = array('rest' => '0', 'info' => 'Application Param Error');
							$rst = json_encode($rst);
							echo $rst;
						}
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Error Image Not Found');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '6' : //IZIN 
					if($this->input->post('caption') || $this->input->post('nik') || $this->input->post('jenis')){
						$caption = $this->input->post('caption');
						$nik = $this->input->post('nik');
						$jenis = $this->input->post('jenis');
						$value = $this->CheckIzin($caption, $nik, $jenis);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '7' : //CUTI 
					if($this->input->post('caption') || $this->input->post('nik') || $this->input->post('tanggal_awal') || 
						$this->input->post('tanggal_akhir') || $this->input->post('lokasi_cuti')){
						$caption = $this->input->post('caption');
						$nik = $this->input->post('nik');
						$tanggal_awal = $this->input->post('tanggal_awal');
						$tanggal_akhir = $this->input->post('tanggal_akhir');
						$lokasi_cuti = $this->input->post('lokasi_cuti');
						$value = $this->CheckCuti($caption, $nik, $tanggal_awal, $tanggal_akhir, $lokasi_cuti);
						echo $value;
					} else{
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
						$rst = json_encode($rst);
						echo $rst;
					}
				break;
				case '10' : //CEK ABSEN IN
					if($this->input->post('nik')){
						$nik = $this->input->post('nik');
						$value = $this->getUserCheckedInById($nik);				
						echo $value;					
					} else {
						$rst = array('rest' => '0', 'info' => 'Application Param Error');
                                                $rst = json_encode($rst);
                                                echo $rst;
					}
				break;
				default:
					$rst = array('rest' => '0', 'info' => 'Application Param DataApi Error');
					$rst = json_encode($rst);
					echo $rst;
			}
		} else{
			$rst = array('rest' => '0', 'info' => 'Application Not Permitted');
			$rst = json_encode($rst);
			echo $rst;
		}
	}

	public function CheckIn($image, $caption, $lat, $long, $nik) { //dataapi = 1
		$config['upload_path']      = '/var/www/html/api_absen/uploads';
	        $config['allowed_types']    = 'jpeg|jpg|png';
         	$config['max_size']         = 1024 * 10;
		$config['overwrite']        = TRUE;
         	$this->load->library('upload', $config);
         	$this->upload->initialize($config);

		$target_path = "/var/www/html/api_absen/uploads/";
		$target_path = $target_path . basename($_FILES['image']['name']);
		$get_path = "/uploads/" . basename($_FILES['image']['name']);

		if(!$this->upload->do_upload('image')){
	             	$info[] = array('status' => strip_tags($this->upload->display_errors()));
                	$rst = array('rest' => '0','info' => $info);
         	} else{
			$upload_data = $this->upload->data();
			$filename = $upload_data['file_name'];
		//}
		
		//if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                	//$rst = array('rest' => '0','info' => 'Could not move the file!');
        	//} else{
			//HITUNG SELISIH WAKTU ABSEN
			$awal  = date_create();
    			$jam_masuk = date_create('08:00:00');
    			$jam_pulang = date_create('17:00:00');
    			$diff_awal  = date_diff( $awal, $jam_masuk );
    			$diff_akhir  = date_diff( $awal, $jam_pulang );
    			$ktrIn = '';
    			$ktrOut = '';
    			if($awal < $jam_masuk)
        			$ktrIn = '-';
    			elseif($awal < $jam_pulang)
        			$ktrOut = '-';
    			else{
        			$ktrIn = '';
        			$ktrOut = '';
    			}

			$id_pegawai = $this->Api_model->getIDPegawai($nik);
			$keterangan = 'h';
			$selisih = $ktrIn. ' '.$diff_awal->h.':'.$diff_awal->i.':'.$diff_awal->s;
			$data = array(
				'id_pegawai' => $id_pegawai,
				'ket_in' => $selisih,
				'image_in' => $get_path,
				'caption_in' => $caption,
				'latitude_in' => $lat,
				'longitude_in' => $long,
				'status_in' => '0',
				'keterangan' => $keterangan
			);
			$status_checkin = $this->Api_model->checkInsertExistCheckin($id_pegawai);
			if($status_checkin == 2 || $status_checkin == 3 || $status_checkin == 4){ 
				//$this->Api_model->checkInsertExistCheckin($id_pegawai) == 4){	
				$last_id_checkin = $this->Api_model->getLastIdChecked($id_pegawai);
				$save = $this->Api_model->updateAbsenIn($data, $last_id_checkin);
                        	if($save):
					//$lastid = $this->db->insert_id();
					$passData = array(
                                        	"nik" => $nik
                                        );
                                        $nik_atasan = $this->getNikAtasan($passData);
					$dataPesan = array(
						'jns_pesan'	=> '0',
						'type_pesan' 	=> 'IN',
						'id_absen' 	=> $last_id_checkin,
						'dari'		=> $nik,
						'kepada'	=> $nik_atasan,
						'proses_pesan'  => '0',
					);
                                	$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
					$pesandecode = json_decode(json_encode($pesan), true);
					if($pesandecode['rst'] == 1){
						$rst = array('rest' => '1', 'info' => 'User successfully Check in!');
					} else{
	                            		$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked in!');
	                            		//$rst = array('rest' => '0', 'info' => $pesandecode);
					}
	                        else:
	                            	$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked in!');
	                        endif;
			} else if($status_checkin == 0){
	                        $rst = array('rest' => '0', 'info' => 'The Previous Request Has Not Been Approved');
			} else if($status_checkin == 1){
	                        $rst = array('rest' => '0', 'info' => 'User already checked in!');
			}
			
		}
                $rst = json_encode($rst);
                return $rst;
        }
	
	public function getNikAtasan($_param){
		$_url = 'http://10.0.1.10/api/web_service/api/11/NIK/0/0/0';
		$postData = '';
        	//create name value pairs seperated by &
	        foreach($_param as $k => $v) { 
	        	$postData .= $k . '='.$v.'&'; 
	        }
	        rtrim($postData, '&');

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL,$_url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	        curl_setopt($ch, CURLOPT_HEADER, FALSE); 
	        curl_setopt($ch, CURLOPT_POST, TRUE);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
	
	        $output=curl_exec($ch);
	        curl_close($ch);
		$json_output = json_decode($output, true);
	        return $json_output['nik'];
	}

	public function getTokenAtasan($_param){
		$_url = 'http://10.0.1.10/api/web_service/api/11/NIK/0/0/0';
		$postData = '';
        	//create name value pairs seperated by &
	        foreach($_param as $k => $v) { 
	        	$postData .= $k . '='.$v.'&'; 
	        }
	        rtrim($postData, '&');

	        $ch = curl_init();
	        curl_setopt($ch, CURLOPT_URL,$_url);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	        curl_setopt($ch, CURLOPT_HEADER, FALSE); 
	        curl_setopt($ch, CURLOPT_POST, TRUE);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
	
	        $output=curl_exec($ch);
	        curl_close($ch);
		$json_output = json_decode($output, true);
	        return $json_output['token'];
	}

	public function getAbsenPesanMasukProgress($nik,$level) { //dataapi = 21
		//$id_pegawai = $this->Api_model->getIDPegawai($nik);
		$qry = $this->Api_model->getAbsenPesanMasukProgress($nik,$level);
		if($qry == 'NULL') :
			$rst = array('rest' => '0','info' => 'No message found');
		else:
			$rst = array('rest' => '1','info' => $qry);
		endif;
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getAbsenPesanMasukHistory($nik,$level) { //dataapi = 22
		$qry = $this->Api_model->getAbsenPesanMasukHistory($nik,$level);
		if($qry == 'NULL') :
			$rst = array('rest' => '0','info' => 'No message found');
		else:
			$rst = array('rest' => '1','info' => $qry);
		endif;
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function viewAbsenPesanMasuk($nik,$id_pesan,$own,$source,$type,$id_type,$jenis_pesan) { //dataapi = 3
		/*
			own => cek apakah baca pesan persetujuan sendiri
			source => 0 (history, 1 progress)
		*/
		$qry = $this->Api_model->viewAbsenPesanMasuk($nik,$id_pesan,$own,$source,$type,$id_type,$jenis_pesan);
		if($qry == 'NULL') :
			$rst = array('rest' => '0','info' => 'No message found');
		else:
			$rst = array('rest' => '1','info' => $qry);
		endif;
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function absenPesanMasukProses($nik,$id_pesan,$approve,$id_type,$type) { //dataapi = 4
		$qry = $this->Api_model->absenPesanMasukProses($nik,$id_pesan,$approve,$id_type,$type);
		if($qry == 'NULL') :
			$rst = array('rest' => '0','info' => 'No message found');
		else:
			$rst = array('rest' => '1','info' => $qry);
		endif;
		$rst = json_encode($rst);
		return $rst;
	}

	public function oneSignal($title){
		$headings = array(
			"en" => 'New Notification SAMPLE'
		);
		$content = array(
		        "en" => $title 
	        );

    		$fields = array(
        		'app_id' => "75dcda13-807d-4ad1-aa30-02ba79882cfd",
	        	//'included_segments' => array('All'),
	        	'data' => array("foo" => "bar"),
		        //'large_icon' =>"ic_launcher_round.png",
			'headings' => $headings,
		        'contents' => $content,
			'include_player_ids' => array("044e3e07-c07c-455d-8635-ae1a31a2aeda"),
			'priority' => '10'
		);

    		$fields = json_encode($fields);
		print("\nJSON sent:\n");
		print($fields);

    		$ch = curl_init();
    		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                	                               	   'Authorization: Basic NGMxNjc4ZGQtYzg5OC00YTEyLWJhY2YtZDFkNWY5M2I4Nzc5'));
    		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    		curl_setopt($ch, CURLOPT_HEADER, FALSE);
    		curl_setopt($ch, CURLOPT_POST, TRUE);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);    

    		$response = curl_exec($ch);
    		curl_close($ch);

    		return $response;
	}

	public function CheckIzin($caption, $nik, $jenis){ //dataapi = 6
		//HITUNG SELISIH WAKTU ABSEN
		$awal  = date_create();
    		$jam_masuk = date_create('08:00:00');
    		$jam_pulang = date_create('17:00:00');
    		$diff_awal  = date_diff( $awal, $jam_masuk );
    		$diff_akhir  = date_diff( $awal, $jam_pulang );
    		$ktrIn = '';
    		$ktrOut = '';
    		if($awal < $jam_masuk)
        		$ktrIn = '-';
    		elseif($awal < $jam_pulang)
        		$ktrOut = '-';
    		else{
        		$ktrIn = '';
        		$ktrOut = '';
    		}
		$id_pegawai = $this->Api_model->getIDPegawai($nik);
		$selisih = $ktrIn. ' '.$diff_awal->h.':'.$diff_awal->i.':'.$diff_awal->s;
		$data = array(
			'id_pegawai' => $id_pegawai,
			'ket_izin' => $selisih,
			'jenis_izin' => $jenis,
			'caption_izin' => $caption,
			'status_izin' => '0'
		);
		$status_checkin = $this->Api_model->checkInsertExistCheckin($id_pegawai);
		$status_checkout = $this->Api_model->checkInsertExistCheckOut($id_pegawai);
		$status_checkizin = $this->Api_model->checkInsertExistCheckIzin($id_pegawai);
		if($status_checkizin == 0 || $status_checkizin == 4){
	        	$rst = array('rest' => '0', 'info' => 'Previous request has not been approved');
		} else{
			$save = $this->Api_model->insertAbsenIzin($data);
	                if($save){
				$lastid = $this->db->insert_id();
				$passData = array(
	                        	"nik" => $nik
	                        );
	                        $nik_atasan = $this->getNikAtasan($passData);
				$dataPesan = array(
					'jns_pesan'	=> '0',
					'type_pesan' 	=> 'IZIN',
					'id_absen' 	=> $lastid,
					'dari'		=> $nik,
					'kepada'	=> $nik_atasan,
					'proses_pesan'  => '0',
				);
				$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
				$pesandecode = json_decode(json_encode($pesan), true);
				if($pesandecode['rst'] == 1){
	                        	$rst = array('rest' => '1', 'info' => 'Waiting for approval!');
				} else{
					$rst = array('rest' => '0', 'info' => 'Unknown error occurred in permission!');
				}
			} else{
		                $rst = array('rest' => '0', 'info' => 'Unknown error occurred in permission!');
			}
		}
		$rst = json_encode($rst);
                return $rst;
	}
	
	public function CheckCuti($caption, $nik, $tanggal_awal, $tanggal_akhir, $lokasi_cuti){ //dataapi = 7
		$id_pegawai = $this->Api_model->getIDPegawai($nik);
		$data = array(
			'id_pegawai' => $id_pegawai,
			'tanggal_awal_cuti' => $tanggal_awal,
			'tanggal_akhir_cuti' => $tanggal_akhir,
			'caption_cuti' => $caption,
			'lokasi_cuti' => $lokasi_cuti,
			'status_cuti' => '0'
		);
		$save = $this->Api_model->insertAbsenCuti($data);
	        if($save){
			$lastid = $this->db->insert_id();
			$passData = array(
	                        "nik" => $nik
	                );
	                $nik_atasan = $this->getNikAtasan($passData);
			$dataPesan = array(
				'jns_pesan'	=> '0',
				'type_pesan' 	=> 'CUTI',
				'id_absen' 	=> $lastid,
				'dari'		=> $nik,
				'kepada'	=> $nik_atasan,
				'proses_pesan'  => '0',
			);
			$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
			$pesandecode = json_decode(json_encode($pesan), true);
			if($pesandecode['rst'] == 1){
	                	$rst = array('rest' => '1', 'info' => 'Waiting for approval!');
			} else{
				$rst = array('rest' => '0', 'info' => 'Unknown error occurred in annual leave!');
			}
		} else{
		        $rst = array('rest' => '0', 'info' => 'Unknown error occurred in annual leave!');
		}
		$rst = json_encode($rst);
                return $rst;
	}

	public function CheckOut($image, $caption, $lat, $long, $nik) { //dataapi = 5
		$target_path = "/var/www/html/api_absen/uploads/";
		$target_path = $target_path . basename($_FILES['image']['name']);
		$get_path = "/uploads/" . basename($_FILES['image']['name']);
		if (!move_uploaded_file($_FILES['image']['tmp_name'], $target_path)) {
                	$rst = array('rest' => '0','info' => 'Could not move the file!');
        	} else{
			//HITUNG SELISIH WAKTU ABSEN
			$awal  = date_create();
    			$jam_masuk = date_create('08:00:00');
    			$jam_pulang = date_create('17:00:00');
    			$diff_awal  = date_diff( $awal, $jam_masuk );
    			$diff_akhir  = date_diff( $awal, $jam_pulang );
    			$ktrIn = '';
    			$ktrOut = '';
    			if($awal < $jam_masuk)
        			$ktrIn = '-';
    			elseif($awal < $jam_pulang)
        			$ktrOut = '-';
    			else{
        			$ktrIn = '';
        			$ktrOut = '';
    			}

			$id_pegawai = $this->Api_model->getIDPegawai($nik);
			$keterangan = 'h';
			$selisih = $ktrOut. ' '.$diff_akhir->h.':'.$diff_akhir->i.':'.$diff_akhir->s;
			$data = array(
				'id_pegawai' => $id_pegawai,
				'ket_out' => $selisih,
				'image_out' => $get_path,
				'caption_out' => $caption,
				'latitude_out' => $lat,
				'longitude_out' => $long,
				'status_out' => '0',
				'keterangan' => $keterangan
			);
			/*
	                    return 0 = belum di approve, jadi data checkout tidak boleh di insert
                	    return 1 = sudah di approve atau absen in pake pintu, harus update data pada row tersebut
        	            return 2 = data checkin tidak di approve, data checkout harus insert data baru
	                    return 3 = belum ada data, harus insert data checkout baru
                	*/
			$status_checkin = $this->Api_model->checkInsertExistCheckin($id_pegawai);
			echo $status_checkin;
			$status_checkout = $this->Api_model->checkInsertExistCheckOut($id_pegawai);
			echo $status_checkout;
			if($status_checkin == 3){	
				if($status_checkout == 0){
	                        	$rst = array('rest' => '0', 'info' => 'The Previous Request Has Not Been Approved');
				} else if($status_checkout == 1){
	                        	$rst = array('rest' => '0', 'info' => 'User already checked out!');
				} /*else if($status_checkout == 2 || $status_checkout == 3){ //insert data baru
					$save = $this->Api_model->insertAbsenOut($data);
	                        	if($save):
						$lastid = $this->db->insert_id();
						$passData = array(
	                                        	"nik" => $nik
	                                        );
	                                        $nik_atasan = $this->getNikAtasan($passData);
						$dataPesan = array(
							'jns_pesan'	=> '0',
							'type_pesan' 	=> 'OUT',
							'id_absen' 	=> $lastid,
							'dari'		=> $nik,
							'kepada'	=> $nik_atasan,
							'proses_pesan'  => '0',
						);
	                                	$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
						$pesandecode = json_decode(json_encode($pesan), true);
						if($pesandecode['rst'] == 1){
		                            		$rst = array('rest' => '1', 'info' => 'User successfully Check Out!');
						} else{
		                            		$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
						}
		                        else:
		                            	$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
		                        endif;
				}*/ else if($status_checkout == 2 || $status_checkout == 3 || $status_checkout == 4){ //update data yang udah masuk
					$last_id_checkin = $this->Api_model->getLastIdCheckin($id_pegawai);
					$update = $this->Api_model->updateAbsenOut($data,$last_id_checkin);
					if($update):
						$passData = array(
	                                        	"nik" => $nik
	                                        );
	                                        $nik_atasan = $this->getNikAtasan($passData);
						$dataPesan = array(
							'jns_pesan'	=> '0',
							'type_pesan' 	=> 'OUT',
							'id_absen' 	=> $last_id_checkin,
							'dari'		=> $nik,
							'kepada'	=> $nik_atasan,
							'proses_pesan'  => '0',
						);
	                                	$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
						//echo $pesan;
						$pesandecode = json_decode(json_encode($pesan), true);
						if($pesandecode['rst'] == 1){
		                            		$rst = array('rest' => '1', 'info' => 'User successfully Check Out!');
						} else{
		                            		$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
						}
					else:
		                            	$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
					endif;
				}
			} else if($status_checkin == 0 || $status_checkin == 2 || $status_checkin == 4){
	                        $rst = array('rest' => '0', 'info' => 'The Previous Request Has Not Been Approved');
			} else if($status_checkin == 1){
				if($status_checkout == 0){
	                        	$rst = array('rest' => '0', 'info' => 'The Previous Request Has Not Been Approved');
				} else if($status_checkout == 1){
	                        	$rst = array('rest' => '0', 'info' => 'User already checked out!');
				} /*else if($status_checkout == 2 || $status_checkout == 3){ //insert data baru
					$save = $this->Api_model->insertAbsenOut($data);
	                        	if($save):
						$lastid = $this->db->insert_id();
						$passData = array(
	                                        	"nik" => $nik
	                                        );
	                                        $nik_atasan = $this->getNikAtasan($passData);
						$dataPesan = array(
							'jns_pesan'	=> '0',
							'type_pesan' 	=> 'OUT',
							'id_absen' 	=> $lastid,
							'dari'		=> $nik,
							'kepada'	=> $nik_atasan,
							'proses_pesan'  => '0',
						);
	                                	$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
						$pesandecode = json_decode(json_encode($pesan), true);
						if($pesandecode['rst'] == 1){
		                            		$rst = array('rest' => '1', 'info' => 'User successfully Check Out!');
						} else{
		                            		$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
						}
		                        else:
		                            	$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
		                        endif;
				}*/ else if($status_checkout == 2 || $status_checkout == 3 || $status_checkout == 4){ //update data yang udah masuk
					$last_id_checkin = $this->Api_model->getLastIdCheckin($id_pegawai);
					$update = $this->Api_model->updateAbsenOut($data,$last_id_checkin);
					if($update):
						$passData = array(
	                                        	"nik" => $nik
	                                        );
	                                        $nik_atasan = $this->getNikAtasan($passData);
						$dataPesan = array(
							'jns_pesan'	=> '0',
							'type_pesan' 	=> 'OUT',
							'id_absen' 	=> $last_id_checkin,
							'dari'		=> $nik,
							'kepada'	=> $nik_atasan,
							'proses_pesan'  => '0',
						);
	                                	$pesan = $this->Api_model->kirimPesanAbsenSentral($dataPesan);
						//echo $pesan;
						$pesandecode = json_decode(json_encode($pesan), true);
						if($pesandecode['rst'] == 1){
		                            		$rst = array('rest' => '1', 'info' => 'User successfully Check Out!');
						} else{
		                            		$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
						}
					else:
		                            	$rst = array('rest' => '0', 'info' => 'Unknown error occurred in checked out!');
					endif;
				}
			}
			
		}
                $rst = json_encode($rst);
                return $rst;
	}

	public function TestApi($a, $b, $c, $d, $e) {
		switch($a) {
			case '1' : $value = $this->getLgnData($b, $c, $e); // 1 => login
				   echo $value;
			break;
			case '2' : $value = $this->getStatusPenggunaanKendaraan($e); // 2 => status kendaraan
				   echo $value;
			break;
			case '3' : $value = $this->getPesanMasuk($b,$c,$d,$e); // 3 => pesan masuk    -> $c,$d wajib bernilai 0 (nol)
				   echo $value;
			break;
			case '3001' : $value = $this->viewPesanMasuk($b,$c,$d,$e); // 3 => view masuk  ( nik / id_pesan / 0 / keyapi )   -> $d wajib bernilai 0 (nol)
				   echo $value;
			break;
			case '3002' : $value = $this->pesanMasukProses($b,$c,$d,$e);
				   echo $value;
			break;
			case '3003' : $value = $this->getReadyDriver($b,$c,$d,$e); // nik/idpesan/join_atau_single/keyAPI
				   echo $value;
			break;
			case '4' : $value = $this->getFormP2k($b,$e); // 4 => form p2k
				   echo $value;
			break;
			case '5' : $value = $this->saveP2k($b,$e); // 5 => save p2k
				   echo $value;
			break;
			case '6' : $value = $this->getMyP2k($b,$e); // 6 => p2k list
				   echo $value;
			break;
			case '6001' : $value = $this->getP2kAdmin($b,$e); // 6 => p2k admin
				   echo $value;
			break;
			case '60010' : $value = $this->getP2kAdminWait($b,$e); // 6 => p2k admin
				   echo $value;
			break;
			case '6002' : $value = $this->getP2kAdminOpen($b,$c,$e); //nik/id_p2k//ketApi
				   echo $value;
			break;
			case '6003' : $value = $this->getP2kAdminOpenProses($b,$c,$d,$e); //nik/id_p2k/(0/1)/ketApi
				   echo $value;
			break;
			case '6004' : $value = $this->getChangeDriverP2k($b,$e); //
				   echo $value;
			break;
			case '6005' : $value = $this->saveChangeDriverP2k($b,$c,$e); //c = id_driver|idc|id_p2k
				   echo $value;
			break;		
			case '6101' : $value = $this->getP2kAtasan($b,$e); // 6 => p2k admin
				   echo $value;
			break;
			case '10' : $value = $this->changePassword($b,$c,$e); // 10 => password
				   echo $value;
			break;
		}
	}

	public function getLgnData($b, $c, $e) { //$a = 1
		$data = array(
			'uname' => $b,
			'pswd' => $c
		);
		if($e == $this->keyAPI):
			$rst = array('rest' => '0','info' => 'TEST API');
			/*
			$qry = $this->Api_model->getLoginData($data);
			if($qry == 'NULL') :
				$rst = array('rest' => '0','info' => 'Wrong username or password');
			elseif($qry == 'NO') :
				$rst = array('rest' => '0','info' => 'You are not authorized');
			else:
				//$rst = $qry;
				$rst = array('rest' => '1','info' => $qry);
			endif;
			*/
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getStatusPenggunaanKendaraan($e) { //$a = 2
		if($e == $this->keyAPI):
			$qry = $this->Api_model->getStatusPenggunaanKendaraan();
			if($qry == 'NULL') :
				$rst = array('rest' => '0','info' => 'Data not found');
			else:
				//$rst = $qry;
				$rst = array('rest' => '1','info' => $qry);
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getPesanMasuk($b,$c,$d,$e) { //$a = 3
		if($e == $this->keyAPI):
			if($c == '0' && $d == '0'):
				$qry = $this->Api_model->getPesanMasuk($b);
				if($qry == 'NULL') :
					$rst = array('rest' => '0','info' => 'No message found');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function viewPesanMasuk($b,$c,$d,$e) { //$a = 3001 $b = NIK $c = id_pesan $d = 0 $c = KEYAPI
		if($e == $this->keyAPI):
			if($c != '0' && $d == '0'):
				$qry = $this->Api_model->viewPesanMasuk($b,$c);
				if($qry == 'NULL') :
					$rst = array('rest' => '0','info' => 'No message found');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function pesanMasukProses($b,$c,$d,$e) { //$a = 3002 $b = NIK $c = id_pesan $d = 1/0[|nopol|driver][single/join] $e = KEYAPI
		if($e == $this->keyAPI):
			$d = urldecode($d);
			$par = explode('|', $d);
			if($c != '0' && $par[0] == '0' || $par[0] == '1'):
				$qry = $this->Api_model->pesanMasukProses($b,$c,$par[0],$par[1],$par[2],$par[3]);
				if($qry == 'NULL') :
					$rst = array('rest' => '0','info' => 'No message found');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
				//return $b.'|'.$c.'|'.$par[0].'|'.$par[1].'|'.$par[2].'|'.$par[3];
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit...');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getReadyDriver($b,$c,$d,$e) { // nik/idpesan/keyAPI
		if($e == $this->keyAPI):
			if($c != '0'):
				$qry = $this->Api_model->getReadyDriver($b,$c,$d);
				if($qry == 'NULL') :
					$rst = array('rest' => '0','info' => 'No message found');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getFormP2k($b,$e) { //$a = 4
		if($e == $this->keyAPI):
			$qry = $this->Api_model->getProyek();
			if($qry == 'NULL') :
				$rst = array('rest' => '0','info' => 'Data project not found');
			else:
				//$rst = $qry;
				$rst = array('rest' => '1','info' => $qry);
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function saveP2k($b,$e){ //$a = 5			
		if($e == $this->keyAPI):
			$b = urldecode($b);
			$par = explode('|', $b); // 0nik | 1dev | 2unit | 3tgl | 4jam | 5tujuan | 6alamat_tujuan | 7keperluan | 8proyek | 9ket | 10waktu_jemput | 11level
			//var_dump($par);			//http://www.gratika.co.id/api/web_service/api/5/G16001K|16|39|Selasa%2003-10-2017|05:30:00|Rs.%20Graha%20Juanda%20Bekasi|Bekasi%20Timur|Mahestri|1|Ditunggu|0|3/350ea7e2af60c9d3824791dd122272d8
			$nikatasan = $this->Api_model->getatasanp2k($par[1],$par[2],$par[11]);
			
			if($nikatasan==NULL):
				$nikatasan2 = $this->Api_model->getvp($par[1]);
				if($nikatasan2==NULL):
					$nikatasan3 = $this->Api_model->getnikatasanfromdatatabel($par[0]);
					if($nikatasan3 != NULL):
						foreach($nikatasan3 as $nikatasan);
					else:
						$adm = $this->Api_model->getadmdriver();
						foreach($adm as $nikatasan);
					endif;
				else:
					foreach($nikatasan2 as $nikatasan);
				endif;
			else:
				foreach($nikatasan as $nikatasan);
			endif;
			 
			//mencari atasan jika tersedia akan di abaikan jika setatasan pada data tidak kosong
			$cek_set_atasan_p2k = $this->Api_model->getnikatasanfromdatatabel($par[0]);
			if($cek_set_atasan_p2k != NULL):
				foreach($cek_set_atasan_p2k as $tmp_set_atasan_p2k);
				$set_atasan_p2k = array('set_atasan_p2k' => $tmp_set_atasan_p2k->nik);
			else:
				$set_atasan_p2k = array('set_atasan_p2k' => $nikatasan->nik);
			endif;

			$tgl = substr($par[3],-10,10);
			/*$pecah = explode("-",$tgl);
			$tgl1 = $pecah[0];
			$tgl2 = $pecah[1];
			$tgl3 = $pecah[2];
			$tgl = $tgl3.'-'.$tgl2.'-'.$tgl1;*/
			
			$dateLimit = new DateTime($par[4]);
			$dateLimit->add(new DateInterval('P0Y0M0DT0H30M0S')); //PHP 5.3
			$dateLimitBooking = $dateLimit->format('H:i:s');
			$id_p2k = 'P2K'.$this->Api_model->getkodeunik('id_p2k','zdata_p2k');
			
			$data = array(
				'id_p2k' => $id_p2k,
				'dev' => $par[1],
				'unit' => $par[2],
				'nik' => $par[0],
				'nik_atasan' => $set_atasan_p2k['set_atasan_p2k'],
				'tgl_buat' => date('Y-m-d'),
				'jam_buat' => date('H:i:s'),
				'proyek' => mysqli_real_escape_string($this->mysqli_link,$par[8]),
				'tgl_pinjam' => $tgl,
				'hari_pinjam' => implode(" ",array_slice(explode(" ", $par[3]),0,1)),
				'jam_pinjam' => $par[4],
				'jam_limit_booking' => $dateLimitBooking,
				'tujuan' => mysqli_real_escape_string($this->mysqli_link,$par[5]),
				'almt_tujuan' => mysqli_real_escape_string($this->mysqli_link,$par[6]),
				'keperluan' => mysqli_real_escape_string($this->mysqli_link,$par[7]),
				'keterangan' => mysqli_real_escape_string($this->mysqli_link,$par[9]),
				'waktu_jemput' => $par[10],
				'id_join' => '0'
			);
			/*$data = array(
				'id_p2k' => $id_p2k,
				'dev' => $par[1],
				'unit' => $par[2],
				'nik' => $par[0],
				'nik_atasan' => $set_atasan_p2k['set_atasan_p2k'],
				'tgl_buat' => date('Y-m-d'),
				'jam_buat' => date('H:i:s'),
				'proyek' => mysql_real_escape_string($par[8]),
				'tgl_pinjam' => $tgl,
				'hari_pinjam' => implode(" ",array_slice(explode(" ", $par[3]),0,1)),
				'jam_pinjam' => $par[4],
				'jam_limit_booking' => $dateLimitBooking,
				'tujuan' => mysql_real_escape_string($par[5]),
				'almt_tujuan' => mysql_real_escape_string($par[6]),
				'keperluan' => mysql_real_escape_string($par[7]),
				'keterangan' => mysql_real_escape_string($par[9]),
				'waktu_jemput' => $par[10],
				'id_join' => '0'
			);*/
			
			$save = $this->Api_model->p2ksave($data);
			if($save):
				$pesan = $this->Api_model->kirimpesan($data,'P2K');
			    $rst = array('rest' => '1', 'info' => 'SUCCESS');
			else:
			    $rst = array('rest' => '0', 'info' => 'FAILED');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getMyP2k($b,$e) { //$a = 6
		if($e == $this->keyAPI):
			$getaNik = $this->Api_model->getallnik($b);
			$byk = $getaNik['byk'];
			if($byk >= 2):
				$pecah = $getaNik['pecah'];
				$nikB = $pecah[0];
				$nikA = $pecah[1];
			else:
				$nikA = $getaNik['pecah'];
				$nikB = 'NULL';
			endif;
			$qry = $this->Api_model->getp2klist($nikA,$nikB);
			if($qry == 'NULL') :
			        $rst = array('rest' => '0', 'info' => 'Data empty');
			else:
				//$rst = $qry;
				$rst = array('rest' => '1','info' => $qry);
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getP2kAdmin($b,$e) { //$a = 6001 $b=nik $e=keyAPI 
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekadminp2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->getP2kAdmin();
				if($qry == 'NULL') :
						$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getP2kAdminWait($b,$e) { //$a = 6001 $b=nik $e=keyAPI 
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekadminp2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->getP2kAdminWait();
				if($qry == 'NULL') :
						$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getP2kAdminOpen($b,$c,$e) { //nik,id_p2k,keyApi
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekAdminP2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->getP2kAdminOpen($c);
				if($qry == 'NULL') :
						$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getP2kAdminOpenProses($b,$c,$d,$e){ //nik,id_p2k,(0/1)keyApi
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekAdminP2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->getP2kAdminOpenProses($c,$d);
				if($qry == 'NULL') :
					$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	
	public function getChangeDriverP2k($b,$e){
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekAdminP2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->getChangeDriverP2k();
				if($qry == 'NULL') :
					$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function saveChangeDriverP2k($b,$c,$e){
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekAdminP2k($b);
			if($cekAdm == '1'):
				$qry = $this->Api_model->saveChangeDriverP2k(urldecode($c));
				if($qry == 'NULL') :
					$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getP2kAtasan($b,$e) { //$a = 6001 $b=nik $e=keyAPI getP2kAtasan
		if($e == $this->keyAPI):
			$cekAdm = $this->Api_model->cekLevelUser($b);
			//var_dump($cekAdm);
			if($cekAdm['level'] == '1' || $cekAdm['level'] == '2'):
				$qry = $this->Api_model->getP2kAtasan($b,$cekAdm['level'],$cekAdm['div']);
				if($qry == 'NULL') :
						$rst = array('rest' => '0', 'info' => 'Data empty');
				else:
					$rst = array('rest' => '1','info' => $qry);
				endif;
			else:
				$rst = array('rest' => '0', 'info' => 'Application not permit');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function changePassword($b,$c,$e) { //$a = 10
		if($e == $this->keyAPI):
			$qry = $this->Api_model->changePassword($b,$c);
			if($qry == 'NULL') :
				$rst = array('rest' => '0', 'info' => 'Change password error');
			else:
				$rst = array('rest' => '1', 'info' => 'Change password success');
			endif;
		else:
			$rst = array('rest' => '0', 'info' => 'Application not permit');
		endif;	
		$rst = json_encode($rst);
		return $rst;
	}
	
	public function getUserById(){
		$nik = $this->input->get('nik');
		$api = $this->input->get('api');
		if($keyAPI==$api):
			$a = $this->Api_model->getUserById($nik);
			$hasil = json_encode($a);
			header('HTTP/1.1: 200');
			header('Status: 200');
			header('Content-Length: '.strlen($hasil));
			exit($hasil);
		else:
			exit('Data Tidak Ada');
		endif;
	}

	public function getUserCheckedInById($a){
		$id_peg = $this->Api_model->getIDPegawai($a);
		//echo $id_peg;
                $status_checkin = $this->Api_model->getExistCheckin($id_peg);
		//print_r( $status_checkin);
		$rst = json_encode($status_checkin);
		return $rst;
        }	
	
}
