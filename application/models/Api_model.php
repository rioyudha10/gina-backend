<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->keyAPISentral = '350ea7e2af60c9d3824791dd122272d8';
		$this->urlSentral = 'http://10.0.1.90/api_grtk/index.php/web_service/api/';
		date_default_timezone_set("Asia/Jakarta");
	}

	function getIDPegawai($nik) {
		$a = "SELECT id from tb_pegawai WHERE nik = '$nik'";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$send = $data->id;
		} else{
			$send = '0';
		}
        	return $send;
    	}

	function getExistCheckin($id_pegawai){
                /*
                    return 0 = belum di approve
                    return 1 = sudah di approve
                    return 2 = tidak di approve
                    return 3 = belum ada data
                    return 4 = data nya null
                */
                $a = "SELECT IFNULL(status_in,4) as status_in, tanggal_in, absen_in, status_out, tanggal_out, absen_out from tb_absen where id_pegawai = '$id_pegawai' and tanggal_in = date(now())";
                $query = $this->db->query($a);
                if($query->num_rows()>0){
                        foreach($query->result() as $que){
                                //$data = $que;
				$arr[] = array (
				'status_in' => $que->status_in,
				'tanggal_in' => $que->tanggal_in,
				'absen_in' => $que->absen_in,
				'status_out' => $que->status_out,
				'tanggal_out' => $que->tanggal_out,
				'absen_out' => $que->absen_out
				);
                        }
                        $res = $arr;
                        $row = $res;
                } else{
                        $row = 3;
                }
                return $row;
        }

	function checkInsertExistCheckin($id_pegawai){
		/*
		    return 0 = belum di approve
		    return 1 = sudah di approve
		    return 2 = tidak di approve
		    return 3 = belum ada data
		    return 4 = data nya null
		*/
		$a = "SELECT IFNULL(status_in,4) as status_in from tb_absen where id_pegawai = '$id_pegawai' and tanggal_in = date(now())";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$res = $data->status_in;
			$row = $res;
		} else{
			$row = 3;
		}
		return $row;
	}

	function checkInsertExistCheckOut($id_pegawai){
		/*
		    return 0 = belum di approve
		    return 1 = sudah di approve
		    return 2 = tidak di approve
		    return 3 = belum ada data
		    return 4 = data nya null
		*/
		$a = "SELECT IFNULL(status_out,4) as status_out from tb_absen where id_pegawai = '$id_pegawai' and (tanggal_in = date(now()) or tanggal_out = date(now())) or (status_in = 1)";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$res = $data->status_out;
			$row = $res;
		} else{
			$row = 3;
		}
		return $row;
	}

	function checkInsertExistCheckIzin($id_pegawai){
		/*
		    return 0 = belum di approve
		    return 1 = sudah di approve
		    return 2 = tidak di approve
		    return 3 = belum ada data
		    return 4 = data nya null
		*/
		$a = "SELECT IFNULL(status_izin,4) as status_izin from tb_absen_izin where id_pegawai = '$id_pegawai' and tanggal_izin = date(now());";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$res = $data->status_izin;
			$row = $res;
		} else{
			$row = 3;
		}
		return $row;
	}

	function checkInsertExistCheckCuti($id_pegawai){
		/*
		    return 0 = belum di approve
		    return 1 = sudah di approve
		    return 2 = tidak di approve
		    return 3 = belum ada data
		    return 4 = data nya null
		*/
		$a = "SELECT IFNULL(status_cuti,4) as status_cuti from tb_absen_cuti where id_pegawai = '$id_pegawai' and tanggal_cuti = date(now());";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$res = $data->status_cuti;
			$row = $res;
		} else{
			$row = 3;
		}
		return $row;
	}

	function insertAbsenIn($data){
		$a = "INSERT INTO tb_absen(id_pegawai,tanggal_in,absen_in,ket_in,image_in,caption_in,latitude_in,longitude_in,status_in,keterangan) VALUES ('$data[id_pegawai]',NOW(),NOW(),'$data[ket_in]','$data[image_in]','$data[caption_in]','$data[latitude_in]','$data[longitude_in]','$data[status_in]','$data[keterangan]')";
		$this->db->query($a);
		return $this->db->affected_rows();
	}

	function updateAbsenIn($data,$id){
                $a = "UPDATE tb_absen set tanggal_in = NOW(), absen_in = NOW(), ket_in = '$data[ket_in]', image_in = '$data[image_in]', caption_in = '$data[caption_in]', latitude_in = '$data[latitude_in]', longitude_in = '$data[longitude_in]', status_in = '$data[status_in]' where id = '$id'";
                $this->db->query($a);
                return $this->db->affected_rows();
        }
	
	function insertAbsenInIzin($param){
		$a0 = "SELECT * FROM tb_absen_izin where id_izin = '$param[id]'";
		$query = $this->db->query($a0);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$id_pegawai_izin = $data->id_pegawai;
			$tanggal_izin = $data->tanggal_izin;
			$jam_izin = $data->jam_izin;
			$keterangan = $data->jenis_izin;
			$caption_izin = $data->caption_izin;
			$status_izin = $param['approve'];
			$a2 = "UPDATE tb_absen_izin set status_izin = '$param[approve]' where id_izin = '$param[id]'";
			$this->db->query($a2);
			if($this->db->affected_rows() && $status_izin == 1){
				$a = "INSERT INTO tb_absen(id_pegawai,tanggal_in,absen_in,keterangan, caption_in, status_in) VALUES ('$id_pegawai_izin','$tanggal_izin','$jam_izin','$keterangan','$caption_izin','$status_izin')";
				$this->db->query($a);
				$row = $this->db->affected_rows();
			} else{
				$row = 0;
			}
		} else{
			$row = 0;
		}
		return $row;
	}

	function insertAbsenInCuti($param){
		$a0 = "SELECT * FROM tb_absen_cuti where id_cuti = '$param[id]'";
		$query = $this->db->query($a0);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$id_pegawai_cuti = $data->id_pegawai;
			$tanggal_cuti = $data->tanggal_cuti;
			$tanggal_awal_cuti = $data->tanggal_awal_cuti;
			$tanggal_akhir_cuti = $data->tanggal_akhir_cuti;
			$caption_cuti = $data->caption_cuti;
			$lokasi_cuti = $data->lokasi_cuti;
			$status_cuti = $param['approve'];
			$keterangan = 'c';
			$a1 = "UPDATE tb_absen_cuti set status_cuti = '$param[approve]' where id_cuti = '$param[id]'";
			$this->db->query($a1);
			if($this->db->affected_rows() && $status_cuti == 1){
				$date1 = date_create($tanggal_awal_cuti);
				$date2 = date_create($tanggal_akhir_cuti);
				$diff = date_diff($date1, $date2);
				$loop = $diff->format('%d')+1;
				$i = 0;
				while($i < $loop){
					$day_in = date('Y-m-d', strtotime($tanggal_awal_cuti. "+".$i." days"));
					$a2 = "INSERT INTO tb_absen (id_pegawai, tanggal_in, keterangan, caption_in, latitude_in, status_in) VALUES('$id_pegawai_cuti', '$day_in', '$keterangan', '$caption_cuti', '$lokasi_cuti', '$status_cuti')";
					$this->db->query($a2);
					$i++;
				}
				$row = 1;
			} else {
				$row = 0;
			}
		} else{
			$row = 0;
		}
		return $row;
	}
	
	function updateAbsenInOut($data){
		if($data['type'] == 'IN'){
			$a = "UPDATE tb_absen set status_in = '$data[approve]',keterangan='h' where id = '$data[id]'";
		} else if($data['type'] == 'OUT'){
			$a = "UPDATE tb_absen set status_out = '$data[approve]' where id = '$data[id]'";
		}
		$this->db->query($a);
		return $this->db->affected_rows();
	}
	
	function insertAbsenOut($data){
		$a = "INSERT INTO tb_absen(id_pegawai,tanggal_out,absen_out,ket_out,image_out,caption_out,latitude_out,longitude_out,status_out,keterangan) VALUES ('$data[id_pegawai]',NOW(),NOW(),'$data[ket_out]','$data[image_out]','$data[caption_out]','$data[latitude_out]','$data[longitude_out]','$data[status_out]','$data[keterangan]')";
		$this->db->query($a);
		return $this->db->affected_rows();
	}

	function insertAbsenIzin($data){
		$a = "INSERT INTO tb_absen_izin(id_pegawai,tanggal_izin,jam_izin,jenis_izin,caption_izin,status_izin) VALUES ('$data[id_pegawai]',NOW(),NOW(),'$data[jenis_izin]','$data[caption_izin]','$data[status_izin]')";
		$this->db->query($a);
		return $this->db->affected_rows();
	}

	function insertAbsenCuti($data){
		$a = "INSERT INTO tb_absen_cuti(id_pegawai,tanggal_cuti,tanggal_awal_cuti,tanggal_akhir_cuti,caption_cuti,lokasi_cuti,status_cuti) VALUES ('$data[id_pegawai]',NOW(),'$data[tanggal_awal_cuti]','$data[tanggal_akhir_cuti]','$data[caption_cuti]','$data[lokasi_cuti]','$data[status_cuti]')";
		$this->db->query($a);
		return $this->db->affected_rows();
	}

	function updateAbsenOut($data,$id){
		$a = "UPDATE tb_absen set tanggal_out = NOW(), absen_out = NOW(), ket_out = '$data[ket_out]', image_out = '$data[image_out]', caption_out = '$data[caption_out]', latitude_out = '$data[latitude_out]', longitude_out = '$data[longitude_out]', status_out = '$data[status_out]' where id = '$id'";
		$this->db->query($a);
		return $this->db->affected_rows();
	}
	
	function kirimpesanAbsen($data){
		$this->dbsimpok = $this->load->database('simpok',TRUE);
		$a = "INSERT INTO tb_pesan(jns_pesan,type_pesan,id_absen,dari,kepada,date,token_dari,token_kepada) VALUES('$data[jns_pesan]','$data[type_pesan]','$data[id_absen]','$data[dari]','$data[kepada]',NOW(),'$data[token_dari]','$data[token_kepada]')";
		$this->dbsimpok->query($a);
	}

	function kirimPesanAbsenSentral($data){
                $paramGet = '11001/0/0/'.$data['type_pesan'].'|'.$data['jns_pesan'].'|'.$data['id_absen'].'|'.$data['dari'].'|'.$data['kepada'].'|'.$data['proses_pesan'].'/'.$this->keyAPISentral;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$this->urlSentral . $paramGet);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                //curl_setopt($ch, CURLOPT_HEADER, FALSE);
                //curl_setopt($ch, CURLOPT_POST, TRUE);
                //curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      
                $output=curl_exec($ch);
                curl_close($ch);
                $json_output = json_decode($output, true);
                return $json_output;
        }

	function getBodyNotif($nik){
		$passData = array(
                	"nik" => $nik
                );
                $_url = 'http://10.0.1.10/api/web_service/api/11/NIK/0/0/0';
                $postData = '';
                //create name value pairs seperated by &
                foreach($passData as $k => $v) {
                        $postData .= $k . '='.$v.'&';
                }
                rtrim($postData, '&');

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      
                $output=curl_exec($ch);
                curl_close($ch);
                $json_output = json_decode($output, true);
                return $json_output['nama'];
        }

	function getTokenAtasan($nik){
		$passData = array(
                	"nik" => $nik
                );
                $_url = 'http://10.0.1.10/api/web_service/api/11/NIK/0/0/0';
                $postData = '';
                //create name value pairs seperated by &
                foreach($passData as $k => $v) {
                        $postData .= $k . '='.$v.'&';
                }
                rtrim($postData, '&');

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      
                $output=curl_exec($ch);
                curl_close($ch);
                $json_output = json_decode($output, true);
                return $json_output['token_atas'];
        }

	function getTokenBawahan($nik){
		$passData = array(
                	"nik" => $nik
                );
                $_url = 'http://10.0.1.10/api/web_service/api/11/NIK/0/0/0';
                $postData = '';
                //create name value pairs seperated by &
                foreach($passData as $k => $v) {
                        $postData .= $k . '='.$v.'&';
                }
                rtrim($postData, '&');

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
      
                $output=curl_exec($ch);
                curl_close($ch);
                $json_output = json_decode($output, true);
                return $json_output['token_bawah'];
        }

	function kirimNotifikasi($nik,$jenis_pesan,$data){
		if($jenis_pesan == '0'){
			$body = $this->getBodyNotif($nik).' Need Approval';
			$player_id = $this->getTokenAtasan($nik); 
		} else if($jenis_pesan == '1'){
			if($data['proses'] == '1'){
				$body = 'Your Process Has Been Approved';
			} else if($data['proses'] == '2'){
				$body = 'Your Process Has Been Decline';
			}
			$player_id = $data['token_kepada'];
		}
                $headings = array(
                        "en" => 'New Notification'
                );
                $content = array(
                        "en" => $body
                );
                $fields = array(
                        'app_id' => "75dcda13-807d-4ad1-aa30-02ba79882cfd",
                        //'included_segments' => array('All'),
                        'data' => array("foo" => "bar"),
                        //'large_icon' =>"ic_launcher_round.png",
                        'headings' => $headings,
                        'contents' => $content,
                        'include_player_ids' => array("$player_id"),
			'priority' => '10'
                );
                $fields = json_encode($fields);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                                           'Authorization: Basic NGMxNjc4ZGQtYzg5OC00YTEyLWJhY2YtZDFkNWY5M2I4Nzc5'));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                return $player_id;
        }

	function getAbsenPesanMasukProgress($nik,$level){
		if($level == 3){
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE dari = '$nik' AND baca = 'N' ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => 'Attendance',
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr = 'NULL';
			endif;
		} else if($level == 2){
			//KE MANAGER
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND baca = 'N' AND jns_pesan = '0' ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr1[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => $c2->nama_pegawai,
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr1 = 'NULL';
			endif;
			//KE VP
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE dari = '$nik' AND baca = 'N' AND jns_pesan = '0' ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr2[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => 'Attendance',
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr2 = 'NULL';
			endif;
			if($arr1 == 'NULL'){
				$arr = $arr2;
			} else if($arr2 == 'NULL'){
				$arr = $arr1;
			} else{
				$arr = array_merge($arr1,$arr2);
			}
		} else if($level == 1){
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND baca = 'N' ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr[] = array (
							'id' => $c->id_pesan,
							'type' => $c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => $c2->nama_pegawai,
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr = 'NULL';
			endif;
		} else {
			$arr = 'NULL';
		}
		return $arr;
	}

	function getAbsenPesanMasukHistory($nik,$level){
		if($level == 3){
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND date > date_add(now(), interval -14 day) ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => 'Attendance',
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr = 'NULL';
			endif;
		} else if($level == 2){
			//APPROVAL DARI BAWAHAN KE MANAGER
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND baca = 'Y' AND date > date_add(now(), interval -14 day) ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr1[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => $c2->nama_pegawai,
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr1 = 'NULL';
			endif;
			//PERMINTAAN APPROVAL DARI MANAGER KE VP
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan a left join tb_absen b on a.id_absen = b.id WHERE a.dari = '$nik' AND a.jns_pesan = '0' AND a.date > date_add(now(), interval -14 day) AND (b.status_in != '0' or b.status_out != '0') ORDER BY a.id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr2[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => 'Attendance',
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr2 = 'NULL';
			endif;
			if($arr1 == 'NULL'){
				$arr = $arr2;
			} else if($arr2 == 'NULL'){
				$arr = $arr1;
			} else{
				$arr = array_merge($arr1,$arr2);
			}
		} else if($level == 1){
			$id_pegawai = $this->getIDPegawai($nik);
			$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND baca = 'Y' ORDER BY id_pesan DESC";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$a2 = "SELECT nama_pegawai FROM tb_pegawai WHERE nik = '$c->dari'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
						$arr[] = array (
							'id' => $c->id_pesan,
							'type' => 'ATTENDANCE-'.$c->type_pesan,
							'jns' => $c->jns_pesan,
							'id_type' => $c->id_absen,
							'nik_dari' => $c->dari,
							'dari' => $c2->nama_pegawai,
							'waktu' => $c->date
						);
				endforeach;
			else:
				$arr = 'NULL';
			endif;
		} else {
			$arr = 'NULL';
		}
		return $arr;
	}

	function viewAbsenPesanMasuk($nik,$id_pesan,$own,$source,$type,$id_type,$jenis_pesan){
		$arr = 'NULL';
		if($own == '1'){
		//kalau manager buka progres sendiri
			/*
			if($source=='0'){ //in progress
				$a = "SELECT * FROM tb_pesan WHERE dari = '$nik' AND baca = 'N' AND id_pesan = '$id'";
			} else{ //history
				$a = "SELECT * FROM tb_pesan WHERE dari = '$nik' AND baca = 'Y' AND id_pesan = '$id'";
			}
			$b = $this->db->query($a);
			if($b->num_rows()>0):
			*/
				//foreach($b->result() as $c):				
			$a2 = "SELECT id, id_pegawai, tanggal_in, absen_in, ket_in, tanggal_out, absen_out, 
				ket_out, keterangan, image_in, image_out, caption_in, caption_out, latitude_in, 
				latitude_out, longitude_in, longitude_out, status_in, status_out from tb_absen
				WHERE id = '$id_type'";
			$b2 = $this->db->query($a2);
			foreach($b2->result() as $c2){
				if($type == 'IN'){
					$arr = array (
						'id_pesan' => $id_pesan,
						'type_pesan' => $type,
						'jns_pesan' => $jenis_pesan,
						'id_absen' => $id_type,
						'tanggal_in' => $c2->tanggal_in,
						'absen_in' => $c2->absen_in,
						'image_in' => $c2->image_in,
						'caption_in' => $c2->caption_in,
						'latitude_in' => $c2->latitude_in,
						'longitude_in' => $c2->longitude_in,
						'status_in' => $c2->status_in,
					);
				} else if($c->type_pesan == 'OUT'){
					$arr = array (
						'id_pesan' => $id_pesan,
						'type_pesan' => $type,
						'jns_pesan' => $jenis_pesan,
						'id_absen' => $id_type,
						'tanggal_out' => $c2->tanggal_out,
						'absen_out' => $c2->absen_out,
						'image_out' => $c2->image_out,
						'caption_out' => $c2->caption_out,
						'latitude_out' => $c2->latitude_out,
						'longitude_out' => $c2->longitude_out,
						'status_out' => $c2->status_out,
					);
				}
			}
						/*
						if($c->jns_pesan == '1'):
							$a3 = "UPDATE tb_pesan SET baca = 'Y' WHERE id_pesan = '$c->id_pesan'";
							$b3 = $this->db->query($a3);
						endif;
						*/
				//endforeach;
			/*
			else:
				$arr = 'NULL';
			endif;
			*/
		} else if($own == '0'){
			//kalau manager buka progres persetujuan bawahan
			//source 0/1 sama aja
			if($type == 'IZIN'){
				$izin = "SELECT id_izin, id_pegawai, tanggal_izin, jam_izin, ket_izin, jenis_izin, caption_izin, status_izin, 
					  proses_izin from tb_absen_izin WHERE id_izin = '$id_type'";
				$izin2 = $this->db->query($izin);
				foreach($izin2->result() as $izin3){
					$arr = array (
						'id_pesan' => $id_pesan,
						'type_pesan' => $type,
						'jns_pesan' => $jenis_pesan,
						'id_izin' => $id_type,
						'tanggal_izin' => $izin3->tanggal_izin,
						'jam_izin' => $izin3->jam_izin,
						'jenis_izin' => $izin3->jenis_izin,
						'caption_izin' => $izin3->caption_izin,
						'status_izin' => $izin3->status_izin,
					);
				}
			} else if($type == 'CUTI'){
				$cuti = "SELECT id_cuti, id_pegawai, tanggal_cuti, tanggal_awal_cuti, tanggal_akhir_cuti, caption_cuti, lokasi_cuti, 
					  status_cuti from tb_absen_cuti WHERE id_absen = '$id_type'";
				$cuti2 = $this->db->query($cuti);
				foreach($cuti2->result() as $cuti3){
					$arr = array (
						'id_pesan' => $id_pesan,
						'type_pesan' => $type,
						'jns_pesan' => $jenis_pesan,
						'id_cuti' => $id_type,
						'tanggal_cuti' => $cuti3->tanggal_cuti,
						'tanggal_awal_cuti' => $cuti3->tanggal_awal_cuti,
						'tanggal_akhir_cuti' => $cuti3->tanggal_akhir_cuti,
						'caption_cuti' => $cuti3->caption_cuti,
						'lokasi_cuti' => $cuti3->lokasi_cuti,
						'status_cuti' => $cuti3->status_cuti,
					);
				}
			} else if($type == 'IN' || $type == 'OUT'){
					$a2 = "SELECT id, id_pegawai, tanggal_in, absen_in, ket_in, tanggal_out, absen_out, 
						ket_out, keterangan, image_in, image_out, caption_in, caption_out, latitude_in, 
						latitude_out, longitude_in, longitude_out, status_in, status_out from tb_absen 
						WHERE id = '$id_type'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2){
						if($type == 'IN'){
							$arr = array (
								'id_pesan' => $id_pesan,
								'type_pesan' => $type,
								'jns_pesan' => $jenis_pesan,
								'id_absen' => $id_type,
								'tanggal_in' => $c2->tanggal_in,
								'absen_in' => $c2->absen_in,
								'image_in' => $c2->image_in,
								'caption_in' => $c2->caption_in,
								'latitude_in' => $c2->latitude_in,
								'longitude_in' => $c2->longitude_in,
								'status_in' => $c2->status_in,
							);
						} else if($type == 'OUT'){
							$arr = array (
								'id_pesan' => $id_pesan,
								'type_pesan' => $type,
								'jns_pesan' => $jenis_pesan,
								'id_absen' => $id_type,
								'tanggal_out' => $c2->tanggal_out,
								'absen_out' => $c2->absen_out,
								'image_out' => $c2->image_out,
								'caption_out' => $c2->caption_out,
								'latitude_out' => $c2->latitude_out,
								'longitude_out' => $c2->longitude_out,
								'status_out' => $c2->status_out,
							);
						}
					}
					/*
					if($c->jns_pesan == '1'):
						$a3 = "UPDATE tb_pesan SET baca = 'Y' WHERE id_pesan = '$c->id_pesan'";
						$b3 = $this->db->query($a3);
					endif;
					*/
			}	
		}
		return $arr;
	}
	
	function absenPesanMasukProses($nik, $id_pesan, $approve, $id_type, $type){
		//$a = "SELECT * FROM tb_pesan WHERE kepada = '$nik' AND baca = 'N' AND id_pesan = '$id_pesan' AND jns_pesan != '1'";
		//$b = $this->db->query($a);
		//if($b->num_rows() > 0): //jika pesan belum di buka, ini untuk atasan
			/*
			foreach($b->result() as $c):
				$id_absen = $c->id_absen;
				$type_pesan = $c->type_pesan;
				$dari = $c->dari;
				$kepada = $c->kepada;
				$token_dari = $c->token_dari;
				$token_kepada = $c->token_kepada;
			*/
				
				//UPDATE TB_ABSEN
				if($type == 'IN'){
					//$a2 = "UPDATE tb_absen_test set status_in = '$approve' where id = '$id_absen'";
					$data = array(
							'type'		=> $type,
							'approve'	=> $approve,
							'id'		=> $id_type
						);
					$b2 = $this->updateAbsenInOut($data);
				} else if($type == 'OUT'){
					//$a2 = "UPDATE tb_absen_test set status_out = '$approve' where id = '$id_absen'";
					$data = array(
							'type'		=> $type,
							'approve'	=> $approve,
							'id'		=> $id_type
						);
					$b2 = $this->updateAbsenInOut($data);
				} else if($type == 'IZIN'){
					$data = array(
							'approve'	=> $approve,
							'id'		=> $id_type
						);
					$b2 = $this->insertAbsenInIzin($data);
				} else if($type == 'CUTI'){
					$data = array(
							'approve'	=> $approve,
							'id'		=> $id_type
						);
					$b2 = $this->insertAbsenInCuti($data);
				}

				//$b2 = $this->db->query($a2);
				/*
				if($b2){
					//UPDATE PESAN
					$a3 = "UPDATE tb_pesan set baca = 'Y' where id_pesan = '$id_pesan'";
					$b3 = $this->db->query($a3);

					//KIRIM PESAN KE PEMINTA
					$data = array(
						'jns_pesan'	=> '1',
						'type_pesan' 	=> $type_pesan,
						'id_absen' 	=> $id_absen,
						'dari'		=> $kepada,
						'kepada'	=> $dari,
						'token_dari'	=> $token_kepada,
						'token_kepada'	=> $token_dari
					);
					$this->kirimpesanAbsen($data);
					$data_notif = array(
						'proses'	=> $approve,
						'token_kepada'	=> $token_dari
					);
					$this->kirimNotifikasi($kepada,'1',$data_notif);
					$arr = array('proses' => 'SUCCESS');
				} else{
					$arr = array('proses' => 'GAGAL');
				}	
				*/
			//endforeach;
		//else:
			$arr = 'NULL';	
			if($b2){
                		$paramGet = '11003/'.$nik.'/'.$id_pesan.'/'.$approve.'/'.$this->keyAPISentral;
				$arr = array('proses' => 'SUCCESS');
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$this->urlSentral . $paramGet);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				$output=curl_exec($ch);
				curl_close($ch);
				$json_output = json_decode($output, true);
				$arr = $json_output;
			} else{
				$arr = array('proses' => 'GAGAL');
			}
		//endif;
		return $arr;
	}
	
	function getLastIdCheckin($id_pegawai){
		$a = "SELECT id from tb_absen where id_pegawai = '$id_pegawai' and status_in = 1 and tanggal_in = date(now()) order by id desc limit 1";
		$query = $this->db->query($a);
		if($query->num_rows()>0){
			foreach($query->result() as $que){
				$data = $que;
			}
			$res = $data->id;
			$row = $res;
		} else{
			$row = 0;
		}
		return $row;
	}

	function getLastIdChecked($id_pegawai){
                $a = "SELECT id from tb_absen where id_pegawai = '$id_pegawai' and tanggal_in = date(now()) order by id desc limit 1";
                $query = $this->db->query($a);
                if($query->num_rows()>0){
                        foreach($query->result() as $que){
                                $data = $que;
                        }
                        $res = $data->id;
                        $row = $res;
                } else{
                        $row = 0;
                }
                return $row;
        }

	function getLoginData($data){
		$akses = array('G01416','G11006','G01415','K140138','G15019K','G01412','G16001K','K140056','G16022','G11002');
		if(in_array($data['uname'], $akses)):
			$a = " SELECT username, full_name, password, email, level, divisi, subdiv, "
				." jabatan, setatasan, user_type FROM zuser_login "
				." WHERE aktif = 'Y' AND username = '$data[uname]' AND password = '$data[pswd]' ";
			$b = $this->db->query($a);
			if($b->num_rows()>0):
				foreach($b->result() as $c):
					$send = array(
						'username' => $c->username,
						'full_name' => $c->full_name,
						'password' => $c->password,
						'email' => $c->email,
						'level' => $c->level,
						'divisi' => $c->divisi,
						'nmdivisi' => $this->get_nmdivisi($c->divisi),
						'subdiv' => $c->subdiv,
						'nmsubdiv' => $this->get_nmdivisi($c->subdiv),
						'jabatan' => $c->jabatan,
						'nmjbtn' => $this->get_nmdivisi($c->jabatan),
						'setatasan' => $c->setatasan,
						'user_type' => $c->user_type
					);
				endforeach;
			else:
				$send = 'NULL';  //wrong password
			endif;
		else:
			$send = 'NO';
		endif;
		return $send;
	}
	
	/*
	function getLoginData($data){
                $a = " SELECT username, full_name, password, email, level, divisi, subdiv, "
                        ." jabatan, setatasan, user_type FROM zuser_login "
                        ." WHERE aktif = 'Y' AND username = '$data[uname]' AND password = '$data[pswd]' ";
                $b = $this->db->query($a);
                if($b->num_rows()>0):
                        foreach($b->result() as $c):
                                $send = array(
                                        'username' => $c->username,
                                        'full_name' => $c->full_name,
                                        'password' => $c->password,
                                        'user_type' => $c->user_type
                                );
                        endforeach;
                else:
                        $send = 'NULL';
                endif;
                return $send;
  	}
	*/

	function get_nmdivisi($kd){
		$a = $this->db->query("SELECT nama_dev FROM zdivisi WHERE unix_id = '$kd'");
		foreach($a->result() as $b):
			$anz = $b->nama_dev;
		endforeach;
		return $anz;
	}

	function getUserById($nik){
		$a = " SELECT username, full_name, email, level, divisi, subdiv, "
			." jabatan, setatasan FROM zuser_login WHERE aktif = 'Y' AND username = '$nik' ";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				$send = array(
					'username' => $c->username,
					'full_name' => $c->full_name,
					'email' => $c->email,
					'level' => $c->level,
					'divisi' => $c->divisi,
					'subdiv' => $c->subdiv,
					'jabatan' => $c->jabatan,
					'setatasan' => $c->setatasan
				);
			endforeach;
		else:
			$send = 'NULL';
		endif;
		return $send;
	}
	
	function getStatusPenggunaanKendaraan(){
		$a = " SELECT a.idc, a.nopol noflat, b.nama nmdriver, b.telp FROM zmobil a "
			." LEFT JOIN zdriver b ON a.idc = b.nopol "
			." WHERE a.aktif = 'Y' "
			." ORDER BY b.status Asc, b.nama Asc " ;
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			$no = 1; $nodrv = 1;
			foreach($b->result() as $c):
				$d = " SELECT a.tujuan, c.a_nmproyek, a.keperluan, b.full_name, a.tgl_pinjam, a.jam_pinjam"
					." FROM zdata_p2k a LEFT JOIN zuser_login b"
					." ON a.nik = b.username "
					." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
					." WHERE a.status = 'Setuju' AND a.status_driver = '1' AND a.id_join = '0' AND a.nopol = '$c->idc' ";
				$e = $this->db->query($d);
				if($e->num_rows()>0):
					foreach($e->result() as $f):
						$tujuan = $f->tujuan; $proyek = $f->a_nmproyek; $keperluan = $f->keperluan;
						$nama = $f->full_name; $berangkat = $f->tgl_pinjam.' '.$f->jam_pinjam;
					endforeach;
				else:
					$tujuan = '&nbsp;'; $proyek = '&nbsp;'; $keperluan = '&nbsp;'; $nama = '&nbsp;'; $berangkat = '&nbsp;';
				endif;
				if($c->nmdriver!=NULL || $c->nmdriver!=''): //$nmdriver = $c->nmdriver;
					$arr[]  = array (
						'no' => $no++,
						'tujuan' => $tujuan,
						'proyek' => $proyek,
						'keperluan' => $keperluan,
						'nama' => $nama,
						'nmdriver' => $c->nmdriver,
						'noflat' => $c->noflat,
						'berangkat' => $berangkat,
						'tlp' => $c->telp
					);
				else: $nmdriver = 'No Driver';
					$a = 1;
					$arr[]  = array (
						'no' => $no++,
						'tujuan' => $tujuan,
						'proyek' => $proyek,
						'keperluan' => $keperluan,
						'nama' => $nama,
						'nmdriver' => 'No Driver '.$nodrv++,
						'noflat' => $c->noflat,
						'berangkat' => $berangkat,
						'tlp' => ''
					);
				endif;
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getPesanMasuk($nik){
		$a = "SELECT * FROM zpesan WHERE kepada = '$nik' AND baca = 'N' ORDER BY id_pesan DESC";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				$a2 = "SELECT full_name FROM zuser_login WHERE username = '$c->dari'";
				$b2 = $this->db->query($a2);
				foreach($b2->result() as $c2);
				$arr[] = array (
					'id' => $c->id_pesan,
					'type' => $c->type_pesan,
					'jns' => $c->jns_pesan,
					'id_type' => $c->id_type,
					//'dari' => $c->dari,
					'dari' => $c2->full_name,
					'waktu' => $c->date
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function viewPesanMasuk($nik,$id){
		$a = "SELECT * FROM zpesan WHERE kepada = '$nik' AND baca = 'N' AND id_pesan = '$id'";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):				
				if($c->type_pesan == 'P2K'):
					$a2 = "SELECT a.id_p2k, a.hari_pinjam, a.tgl_pinjam, a.jam_pinjam, a.keperluan, a.keterangan, a.status, a.id_join, 
						  b.full_name, c.a_nmproyek, d.nopol, e.nama 
						  FROM zdata_p2k a 
						  LEFT JOIN zuser_login b ON a.nik=b.username 
						  LEFT JOIN zproyek c ON a.proyek=c.a_idproyek
						  LEFT JOIN zmobil d ON a.nopol=d.idc
						  LEFT JOIN zdriver e ON a.id_driver=e.id_driver
						  WHERE a.id_p2k = '$c->id_type'";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2);
					
					if($c2->id_join=='0' && !empty($c2->nopol) && !empty($c2->nama)):
						$type_pinjam = 'Single';
					elseif($c2->id_join!='0' && !empty($c2->nopol) && !empty($c2->nama)):
						$a4 = "SELECT full_name FROM zuser_login WHERE username in (SELECT nik FROM zdata_p2k WHERE id_p2k = '$c2->id_join')";
						$b4 = $this->db->query($a4);
						if($b4->num_rows()>0):
							foreach($b4->result() as $c4):
								$type_pinjam = 'Join to '.$c4->full_name;
							endforeach;
						else:
							$type_pinjam = 'Join to ';
						endif;
					else:
						$type_pinjam = NULL;
					endif;
					
					$arr[] = array (
						'id' => $c2->id_p2k,
						'nama' => $c2->full_name,
						'tgl' => $c2->hari_pinjam.', '.$c2->tgl_pinjam,
						'jam' => $c2->jam_pinjam,
						'proyek' => $c2->a_nmproyek,
						'keperluan' => $c2->keperluan,
						'keterangan' => $c2->keterangan,
						'nopol' => $c2->nopol,
						'driver' => $c2->nama,
						'status' => $c2->status,
						'jns' => $c->jns_pesan,
						'type_pinjam' => $type_pinjam
					);
					
					if($c->jns_pesan == '1'):
						$a3 = "UPDATE zpesan SET baca = 'Y' WHERE id_pesan = '$c->id_pesan'";
						$b3 = $this->db->query($a3);
					endif;
				
				else:
					$arr = 'NULL';
				endif;
			endforeach;
			
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function pesanMasukProses($nik,$id,$proses,$id_kendaraan,$id_driver,$jos){ 
		//return $nik.'|'.$id.'|'.$proses.'|'.$id_kendaraan.'|'.$id_driver.'|'.$jos;
		if($proses == '0'): $status = 'Tidak'; 
		elseif($proses == '1'): $status = 'Setuju'; 
		endif;
		$a = "SELECT * FROM zpesan WHERE kepada = '$nik' AND baca = 'N' AND id_pesan = '$id' AND jns_pesan != '1'";
		$b = $this->db->query($a);
		if($b->num_rows() > 0): //jika pesan belum di buka, ini untuk atasan dan admin
			foreach($b->result() as $c):
				$id_type = $c->id_type;
				$aa = "SELECT nik FROM zdata_p2k WHERE id_p2k = '$id_type'";
				$bb = $this->db->query($aa);
				foreach($bb->result() as $cc):
					$peminjam = $cc->nik;
				endforeach;
				if($c->type_pesan == 'P2K'):
					$data = array(
						'type_pesan' => 'P2K',
						'id_type' => $id_type,
						'dari' => $nik, //yang online
						'kepada' => $peminjam, //JGN DARI PESAN, TAPI DARI P2K
						'baca' => 'N',
						'date' => date('Y-m-d H:i:s')
					);
					//update pesan
					$a3 = "UPDATE zpesan SET baca = 'Y' WHERE kepada = '$nik' AND id_pesan = '$id'";
					$b3 = $this->db->query($a3);
					
					if($c->jns_pesan=='2'):
						if($proses == '0'):
							$a5 = "UPDATE zdata_p2k SET status = '$status' WHERE id_p2k = '$id_type'";
							$b5 = $this->db->query($a5);
						elseif($proses == '1'):
							if($jos=='2'):
								$a6 = "SELECT id_p2k, nik FROM zdata_p2k WHERE nopol = '$id_kendaraan' AND status_driver = '1' ";
								$b6 = $this->db->query($a6);
								foreach($b6->result() as $z6):
									$join = ", id_join = '".$z6->id_p2k."', nik_join = '".$z6->nik."' ";
								endforeach;
							else: 
								$join = ''; 
							endif;				
							$a4 = "UPDATE zdata_p2k SET nopol = '$id_kendaraan', id_driver = '$id_driver', status_driver = '1' $join WHERE id_p2k = '$id_type'";
							$b4 = $this->db->query($a4);
							$a5 = "UPDATE zmobil set digunakanp2k = 'Y' where idc = '$id_kendaraan' ";
							$b5 = $this->db->query($a5);
						endif;
						//kirim pesan ke peminjam
						$this->kirimpesanbalasan($data);
						$arr = array('proses' => 'SUCCESS');
					else:
						$a2 = "UPDATE zdata_p2k SET status = '$status' WHERE id_p2k = '$id_type' AND nik_atasan = '$nik'";
						$b2 = $this->db->query($a2);
						if($b2):
							//kirim balasan
							$this->kirimpesanbalasan($data);
							if($proses == '1'): //JIKA SETUJU
								$this->kirimpesankeadmin($data);
							endif;
							$arr = array('proses' => 'SUCCESS');
						else:
							$arr = 'NULL';
						endif;
					endif;
				else:
					$arr = 'NULL';
				endif;				
			endforeach;
		else: //dibuat untuk proses pembatalan oleh peminjam dan pesan sudah dibuka pada 3001	
			$aa = "SELECT * FROM zpesan WHERE kepada = '$nik' AND id_pesan = '$id' AND jns_pesan = '1'";
			$bb = $this->db->query($aa);
			if($bb->num_rows() > 0):
			//return $aa.' | '.$bb->num_rows();
				foreach($bb->result() as $cc):
					$id_type = $cc->id_type;
					$aa4 = "SELECT id_join, nopol, id_driver FROM zdata_p2k  WHERE id_p2k = '$id_type'";
					$bb4 = $this->db->query($aa4);
					foreach($bb4->result() as $zz4):
					//return $aa4.' | '.$id_type.' | '.$zz4->id_join;
						if($zz4->id_join != '0'): //pengguna ini join kendaraan, maka tidak berpengaruh pada mobil
							$aa5 = "UPDATE zdata_p2k SET status_driver = '3' WHERE id_p2k = '$id_type'";
							$bb5 = $this->db->query($aa5);
						else: 
							$aa6 = "SELECT id_p2k, status_driver FROM zdata_p2k WHERE id_join = '$id_type' ";
							$bb6 = $this->db->query($aa6);
							if($bb6->num_rows() <= 0): //SINGLE DAN GAK ADA YANG IKUT JOIN
								$aa7 = "UPDATE zdata_p2k SET status_driver = '3' WHERE id_p2k = '$id_type'";
								$bb7 = $this->db->query($aa7);
								$aa8 = "UPDATE zmobil SET digunakanp2k = 'N' WHERE idc = '$zz4->nopol'";
								$bb8 = $this->db->query($aa8);
								//return $aa7.' | '.$aa8;
							else:      //SINGLE DAN ADA 1 ATAU LEBIH YANG IKUT JOIN
								foreach($bb6->result() as $zz6):
									if($zz6->status_driver == '1'):
										$aa9 = "UPDATE zdata_p2k SET id_join = '0', nik_join = '' WHERE id_p2k = '$zz6->id_p2k'"; //MERUBAH YANG IKUT JOIN
										$bb9 = $this->db->query($aa9);
										$aa10 = "UPDATE zdata_p2k SET status_driver = '3' WHERE id_p2k = '$id_type'"; //MERUBAH PEMBUAT
										$bb10 = $this->db->query($aa10);
										//return $aa9.' | '.$aa10;
									else:
										$aa11 = "UPDATE zdata_p2k SET status_driver = '3' WHERE id_p2k = '$id_type'"; //MERUBAH PEMBUAT
										$bb11 = $this->db->query($aa11);
										$aa12 = "UPDATE zmobil SET digunakanp2k = 'N' WHERE idc = '$zz4->nopol'";
										$bb12 = $this->db->query($aa12);
										//return $aa11.' | '.$aa12;
									endif;
								endforeach;
								//$ar = array('proses' => 'SINGLE DAN ADA 1 ATAU LEBIH YANG IKUT JOIN');
							endif;
						endif;
					endforeach;
				endforeach;
				$arr = array('proses' => 'SUCCESS');
			else:
				$arr = 'NULL';
			endif;
		endif;
		return $arr;
	}
	
	function getReadyDriver($nik,$id,$jos){
		$nodrv = 1;
		$a = "SELECT * FROM zpesan WHERE kepada = '$nik' AND baca = 'N' AND id_pesan = '$id'";
		$b = $this->db->query($a);
		if($b->num_rows() > 0):
			//return $nik.'|'.$id;
			foreach($b->result() as $c):
				if($c->jns_pesan=='2'):
					if($jos=='1'): $digunakan = 'N'; else: $digunakan = 'Y'; endif;
					$a2 = " SELECT a.idc, a.nopol, b.id_driver, b.nama FROM zmobil a "
						 ." LEFT JOIN zdriver b ON a.idc=b.nopol WHERE digunakanp2k = '$digunakan' AND digunakanpks = 'N' order by b.nama asc";
					$b2 = $this->db->query($a2);
					foreach($b2->result() as $c2):
						if($c2->nama != NULL || $c2->nama != ''):
							$arr[] = array (
								'idc' => $c2->idc,
								'nopol' => $c2->nopol,
								'idd' => $c2->id_driver,
								'nama' => $c2->nama
							);
						else: 
							$nama = 'No Driver';
							$arr[] = array (
								'idc' => $c2->idc,
								'nopol' => $c2->nopol,
								'idd' => $c2->id_driver,
								'nama' => $nama.' '.$nodrv++
							);
						endif;
					endforeach;
				else:
					$arr = 'NULL';
				endif;
			endforeach;
		else:
				$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getProyek(){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$a = "SELECT * FROM zproyek WHERE a_idproyek != '1' order by a_nmproyek asc";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				$arr[] = array (
					'id' => $c->a_idproyek,
					'nama' => $c->a_nmproyek
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getkodeunik($a,$b) {
		//$this->db = $this->load->database('g_simpok', TRUE); 
		$q = $this->db->query("SELECT substr(MAX($a),-5,5) AS ID  FROM $b");
		if($q->num_rows()<0){
			$ID = "00001";
		}else{
			foreach($q->result() as $k);
			$MaksID = $k->ID;
			$MaksID++;
			if($MaksID < 10){ $ID = "0000".$MaksID;}
			else if($MaksID < 100){ $ID = "000".$MaksID;}
			else if($MaksID < 1000){ $ID = "00".$MaksID;}
			else if($MaksID < 10000){ $ID = "0".$MaksID;}
			else { $ID = $MaksID++;}
		}
		return $ID;
    	}
	
	function getatasanp2k($departemen,$unitkerja,$level){
		//$this->db = $this->load->database('g_simpok', TRUE);
		//var_dump('<br>'.$departemen.'|'.$unitkerja.'|'.$level);
		if($level=='3'){
			$where = "WHERE `subdiv` = '$unitkerja' AND `level` = '2'";
		}elseif($level=='2'){
			$where = "WHERE `divisi` = '$departemen' AND `level` = '1'";
		}else{
			$where = "WHERE `divisi` = '$departemen' AND `level` = '0'";
		}
		$a = $this->db->query("SELECT `nik` FROM `zuser_login` $where AND aktif = 'Y'");
		return $a->result();
	}

	function getvp($dev){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$a = $this->db->query("SELECT `nik` FROM `zuser_login` WHERE `divisi` ='$dev' AND `level` = '1' AND aktif = 'Y' ");
		return $a->result();
	}

	function getnikatasanfromdatatabel($nik){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$z = "SELECT setatasan as nik FROM zuser_login WHERE username = '$nik' AND aktif = 'Y' ";
	    $a = $this->db->query($z);
		foreach($a->result() as $b);
		if(!empty($b->nik)):
			$az = $a->result();
		else:
			$az = NULL;
		endif;
	    return $az;
	}

	function getadmdriver(){
	    $query = $this->db->query("SELECT nik FROM zuser_login WHERE user_type = 'adm' ");
	    return $query->result();
    	}

	function p2ksave($data){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$a = "INSERT INTO zdata_p2k(id_p2k,dev,unit,nik,nik_atasan,tgl_buat,jam_buat,proyek,tgl_pinjam,hari_pinjam,jam_pinjam,jam_limit_booking,tujuan,almt_tujuan,keperluan,keterangan,waktu_jemput,id_join) VALUES ('$data[id_p2k]','$data[dev]','$data[unit]','$data[nik]','$data[nik_atasan]','$data[tgl_buat]','$data[jam_buat]','$data[proyek]','$data[tgl_pinjam]','$data[hari_pinjam]','$data[jam_pinjam]','$data[jam_limit_booking]','$data[tujuan]','$data[almt_tujuan]','$data[keperluan]','$data[keterangan]','$data[waktu_jemput]','$data[id_join]')";
		
		//var_dump('<hr>'.$a.'<hr>');
		$this->db->query($a);
		//$this->log_sql($a,$data['nik']);
		return $this->db->affected_rows();
	}
	
	function kirimpesan($data,$type){
		if($type == 'P2K'): 
			$id = $data['id_p2k'];
			$kepada = $data['nik_atasan'];
		elseif($type == 'PKS'):
			$id = $data['id_pks']; 
			$kepada = $data['nik_atasan'];
		endif;
		$a = "INSERT INTO zpesan(type_pesan,id_type,dari,kepada,date) VALUES('$type','$id','$data[nik]','$kepada','$data[tgl_buat] $data[jam_buat]')";
		$this->db->query($a);
	}
	
	function kirimpesanbalasan($data){
		$a = "INSERT INTO zpesan(type_pesan,jns_pesan,id_type,dari,kepada,date) 
			 VALUES('$data[type_pesan]','1','$data[id_type]','$data[dari]','$data[kepada]','$data[date]')";
		$this->db->query($a);
	}

	function kirimpesankeadmin($data){
		$adm = $this->getadmdriver();
		foreach($adm as $adm);
		$admin = $adm->nik;
		$a = " INSERT INTO zpesan(type_pesan,jns_pesan,id_type,dari,kepada,date) "
			." VALUES('$data[type_pesan]','2','$data[id_type]','$data[dari]','$admin','$data[date]')";
		$this->db->query($a);
	}
	
	function getallnik($nik){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$a = "SELECT nik FROM zuser_login WHERE username = '$nik' ";
		$query = $this->db->query($a);
		foreach($query->result() as $nik);
		if(stristr($nik->nik,'|')==TRUE):
			$d['pecah'] = explode("|",$nik->nik);
			$d['byk'] = count($d['pecah']);
		else:
			$d['pecah'] = $nik->nik;
			$d['byk'] = '1';
		endif;
		return $d;
	}
	
	function getp2klist($nik,$nik2){
		//$this->db = $this->load->database('g_simpok', TRUE);
		if($nik2=='NULL'):
			$nikOr = " a.nik = '$nik' ";
		else:
			$nikOr = " (a.nik = '$nik' OR a.nik = '$nik2') ";
		endif;
		//$a = "SELECT * FROM zdata_p2k WHERE $nikOr ORDER BY id_p2k DESC LIMIT 15 ";
		$a = " SELECT a.*, b.full_name, c.a_nmproyek, d.nama, e.nopol, f.full_name as atasan "
			." FROM zdata_p2k a "
			." LEFT JOIN zuser_login b ON a.nik = b.username "
			." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
			." LEFT JOIN zdriver d ON a.id_driver = d.id_driver "
			." LEFT JOIN zmobil e ON a.nopol = e.idc "
			." LEFT JOIN zuser_login f ON a.nik_atasan = f.username "
			." WHERE $nikOr ORDER BY id_p2k DESC LIMIT 15 ";
		$b = $this->db->query($a);		
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				if($c->status=='Tunggu'): $status='1'; elseif($c->status=='Setuju'): $status='2'; elseif($c->status=='Tidak'): $status='3'; endif;
				$arr[] = array (
					'id' => $c->id_p2k,
					'nama' => $c->full_name, //nik,
					'kepada' => $c->atasan, //nik_atasan,
					'berangkat' => $c->tgl_pinjam.' '.$c->jam_pinjam,
					'keperluan' => $c->keperluan,
					'tujuan' => $c->tujuan,
					'proyek' => $c->a_nmproyek, //proyek,
					'keterangan' => $c->keterangan,
					'noflat' => $c->nopol,
					'status' => $status
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function cekAdminP2k($b){
		$a = "SELECT adm_p2k FROM zuser_login WHERE username = '$b' AND adm_p2k = '1'";
		$b = $this->db->query($a);		
		if($b->num_rows()>0):
			$arr = '1';
		else:
			$arr = '0';
		endif;
		return $arr;
	}
	
	function getP2kAdmin(){
		$a = " SELECT a.*, b.full_name, c.a_nmproyek, d.nama, e.nopol, f.full_name as atasan "
			." FROM zdata_p2k a "
			." LEFT JOIN zuser_login b ON a.nik = b.username "
			." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
			." LEFT JOIN zdriver d ON a.id_driver = d.id_driver "
			." LEFT JOIN zmobil e ON a.nopol = e.idc "
			." LEFT JOIN zuser_login f ON a.nik_atasan = f.username "
			." WHERE a.status = 'Setuju' AND a.status_driver = '1' ORDER BY a.tgl_pinjam DESC, a.jam_pinjam DESC, a.id_p2k DESC";
		$b = $this->db->query($a);		
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				if($c->status=='Tunggu'): $status='1'; elseif($c->status=='Setuju'): $status='2'; elseif($c->status=='Tidak'): $status='2'; endif;
				$arr[] = array (
					'id' => $c->id_p2k,
					'peminjam' => $c->full_name,
					'atasan' => $c->atasan,
					'tanggal' => $c->tgl_pinjam.' '.$c->jam_pinjam,
					'keperluan' => $c->keperluan,
					'tujuan' => $c->tujuan,
					'proyek' => $c->proyek,
					'keterangan' => $c->keterangan,
					'kendaraan' => $c->nopol,
					'status' => $status
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getP2kAdminWait(){
		$a = " SELECT a.*, b.full_name, c.a_nmproyek, d.nama, e.nopol, f.full_name as atasan "
			." FROM zdata_p2k a "
			." LEFT JOIN zuser_login b ON a.nik = b.username "
			." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
			." LEFT JOIN zdriver d ON a.id_driver = d.id_driver "
			." LEFT JOIN zmobil e ON a.nopol = e.idc "
			." LEFT JOIN zuser_login f ON a.nik_atasan = f.username "
			." WHERE a.status = 'Tunggu' ORDER BY a.tgl_pinjam DESC, a.jam_pinjam DESC, a.id_p2k DESC";
		$b = $this->db->query($a);		
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				if($c->status=='Tunggu'): $status='1'; elseif($c->status=='Setuju'): $status='2'; elseif($c->status=='Tidak'): $status='2'; endif;
				$arr[] = array (
					'id' => $c->id_p2k,
					'nama' => $c->full_name,
					'kepada' => $c->atasan,
					'berangkat' => $c->tgl_pinjam.' '.$c->jam_pinjam,
					'keperluan' => $c->keperluan,
					'tujuan' => $c->tujuan,
					'proyek' => $c->a_nmproyek,
					'keterangan' => $c->keterangan,
					'noflat' => $c->nopol,
					'status' => $status
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getP2kAdminOpen($id){
		$a = " SELECT a.*, b.full_name, c.a_nmproyek, d.nama, e.nopol "
			." FROM zdata_p2k a "
			." LEFT JOIN zuser_login b ON a.nik = b.username "
			." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
			." LEFT JOIN zdriver d ON a.id_driver = d.id_driver "
			." LEFT JOIN zmobil e ON a.nopol = e.idc "
			." WHERE a.id_p2k = '$id' AND a.status = 'Setuju' AND a.status_driver = '1' ";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				if($c->id_join=='0'): $type_pinjam = 'Single'; else: $type_pinjam = 'Join'; endif;
				$arr[] = array (
					'id' => $c->id_p2k,
					'nama' => $c->full_name,
					'tgl' => $c->hari_pinjam.', '.$c->tgl_pinjam,
					'jam' => $c->jam_pinjam,
					'proyek' => $c->a_nmproyek,
					'keperluan' => $c->keperluan,
					'keterangan' => $c->keterangan,
					'nopol' => $c->nopol,
					'driver' => $c->nama,
					'status' => $c->status,
					'type_pinjam' => $type_pinjam
				);
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getP2kAdminOpenProses($id,$proses){
		if($proses=='1'):
			$status_driver = '2';
		else:
			$status_driver = '3';
		endif;
		$a = "SELECT id_join, nopol FROM zdata_p2k WHERE status_driver = '1' AND id_p2k = '$id'";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $z):
				$a2 = "UPDATE zdata_p2k SET status_driver = '$status_driver' WHERE id_p2k = '$id'" ;
				$b2 = $this->db->query($a2);
				if($z->id_join=='0'):					
					$a3 = "UPDATE zmobil SET digunakanp2k = 'N' WHERE idc = '$z->nopol'" ;
					$b3 = $this->db->query($a3);
				endif;
				$arr = array('proses' => 'SUCCESS');
			endforeach;
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}
	
	function getChangeDriverP2k(){
		$a = " SELECT b.id_driver, b.nama, c.idc, c.nopol, a.id_p2k FROM zdata_p2k a "
			." LEFT JOIN zdriver b ON a.id_driver = b.id_driver "
			." LEFT JOIN zmobil c ON a.nopol = c.idc "
			." WHERE a.status_driver = '1' and a.status = 'Setuju' and a.id_join = '0' "
			." ORDER BY b.nama ASC ";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $ab):
				$arrab[] = array (
					'id' => $ab->id_p2k,
					'value' => $ab->nama.' >> '.$ab->nopol,
				);
			endforeach;
		else:
			$arrab = 'NULL';
		endif;
		
		$c = " SELECT a.idc, a.nopol, b.id_driver, b.nama FROM zmobil a "
			." LEFT JOIN zdriver b ON a.idc = b.nopol "
			." WHERE a.aktif = 'Y' "
			." ORDER BY b.status Asc, b.nama Asc " ;
		$d = $this->db->query($c);
		if($d->num_rows()>0):
			foreach($d->result() as $cd):
				$arrcd[] = array (
					'id' => $cd->id_driver.'|'.$cd->idc,
					'value' => $cd->nama.' >> '.$cd->nopol,
				);
			endforeach;
		else:
			$arrcd = 'NULL';
		endif;
		
		$arr = array(
			'from' => $arrab,
			'to' => $arrcd
		);
		
		return $arr;
	}
	
	function saveChangeDriverP2k($c){ //id_driver|idc|id_p2k
		$dt = explode("|",$c);
		$a = " SELECT id_p2k FROM zdata_p2k WHERE id_p2k = '$dt[2]' AND status_driver = '1' AND status = 'Setuju' AND id_join = '0' ";
		$b = $this->db->query($a);
		if($b->num_rows()==0):
			$arr = 'NULL';
		else:
			foreach($b->result() as $c):
				$d = " UPDATE zdata_p2k SET id_driver = '$dt[0]', nopol = '$dt[1]' WHERE id_p2k = '$c->id_p2k' ";
				$e = $this->db->query($d);
			endforeach;
			if($e==true):
				$arr = 'Change Driver Success';
			else:
				$arr = 'NULL';
			endif;
		endif;
		return $arr;
	}
	
	function cekLevelUser($b){
		$a = " SELECT level, divisi, subdiv FROM zuser_login WHERE aktif = 'Y' AND username = '$b' ";
		$b = $this->db->query($a);		
		if($b->num_rows()>0):
			foreach($b->result() as $z):
				$arr = array(
					'level' => $z->level, // = 1/2/3
					'div' => $z->divisi,
					'subdiv' => $z->subdiv
				);
			endforeach;
		else:
			$arr = '0';
		endif;
		return $arr;
	}
	
	function getP2kAtasan($nik,$lvl,$dev){
		if($lvl=='1'):
			$where = " WHERE a.dev = '$dev' ";
		else:
			$where = " WHERE a.nik_atasan = '$nik' ";
		endif;
		$a = " SELECT a.*, b.full_name, c.a_nmproyek, d.nama, e.nopol, f.full_name as atasan "
			." FROM zdata_p2k a "
			." LEFT JOIN zuser_login b ON a.nik = b.username "
			." LEFT JOIN zproyek c ON a.proyek = c.a_idproyek "
			." LEFT JOIN zdriver d ON a.id_driver = d.id_driver "
			." LEFT JOIN zmobil e ON a.nopol = e.idc "
			." LEFT JOIN zuser_login f ON a.nik_atasan = f.username "
			." $where "
			." ORDER BY a.tgl_pinjam DESC, a.tgl_pinjam ASC limit 20 ";
		$b = $this->db->query($a);
		if($b->num_rows()>0):
			foreach($b->result() as $c):
				if($c->status=='Tunggu'): $status='1'; elseif($c->status=='Setuju'): $status='2'; elseif($c->status=='Tidak'): $status='3'; endif;
				$arr[] = array (
					'id' => $c->id_p2k,
					'nama' => $c->full_name, //nik,
					'kepada' => $c->atasan, //nik_atasan,
					'berangkat' => $c->tgl_pinjam.' '.$c->jam_pinjam,
					'keperluan' => $c->keperluan,
					'tujuan' => $c->tujuan,
					'proyek' => $c->a_nmproyek, //proyek,
					'keterangan' => $c->keterangan,
					'noflat' => $c->nopol,
					'status' => $status
				);
			endforeach;
		else:
			$arr = '0';
		endif;
		return $arr;
	}
	
	function changePassword($b,$c){
		//$this->db = $this->load->database('g_simpok', TRUE);
		$a = "UPDATE zuser_login SET password = '$c' WHERE username = '$b'";
		$b = $this->db->query($a);
		if($b==true):
			$arr = 'OK';
		else:
			$arr = 'NULL';
		endif;
		return $arr;
	}

}
